<?php
require_once 'classes/DB.php';

$dbh = DB::hentDB();

$sql = 'SELECT avatar FROM user WHERE id=?';
$sth = $dbh->prepare ($sql);
$sth->execute([$_GET['id']]);

if ($bilde=$sth->fetch(PDO::FETCH_ASSOC)) {
  header('Content-type: image/png');

  echo $bilde['avatar'];
} else {
  header("HTTP/1.0 404 Not Found");
}