<?php
/**
 * Når brukeren er logget på (enten ved å ha opprettet ny bruker eller logget på ved å kjøre oppgave2.php) 
 * så skal vedkommende ved å gå til oppgave4.php få mulighet til å laste opp avatar bilde (bilde for brukeren).
 *  Lag en form for å la brukeren laste opp bilde. Når brukeren sender bilde så skal dette sendes til samme skriptet. 
 * Skaler bildet til maksimalt 150x150 punkter og lagre det i tabellen i kolonnen avatar for riktig bruker.
 */



 /**
  * TODO
  * Endre utseende på fil knappen
  */
require_once 'vendor/autoload.php';
require_once 'classes/DB.php';
require_once 'classes/Bruker.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader, array(
    /*'cache' => './compilation_cache',*/ /* Only enable cache when everything works correctly */
));

$data = [];
$dbh = DB::hentDB();
$bruker = new Bruker($dbh);

if($bruker->innlogget()) {
  $data['innlogget'] = true;
  if(isset($_FILES['lastOppFil'])) {
    $bruker-> lastOppAvatar($bruker->innlogget(), $_FILES['lastOppFil']);
  }
} 





echo $twig->render('oppgave4.html', $data);
?>