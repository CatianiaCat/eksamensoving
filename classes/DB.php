<?php

/**Klassen er hentet fra Prosjekt 1 HLS */

/**
 * Klasse for å hente ut et PDO-objekt som er koblet til databasen.
 * Man får hentet ut et PDO objekt ved å kalle "DB::hentDB()"
 */
class DB {
  private static $db = null;
  private $dbnavn = 'imt2291eksamen470823';
  private $host = '127.0.0.1';
  private $bruker = 'root';
  private $passord = '';
  private $dbh = null;

  /**
   * Constructor som oppretter et nytt PDO-objekt med detaljene som er satt over.
   * Dersom det ikke er mulig å koble til databasen, vil det prøves å bare koble til database-serveren og opprette databasen.
   */
  private function __construct() {
    try {
        $this->dbh = new PDO("mysql:dbname=$this->dbnavn;host=$this->host", $this->bruker, $this->passord);
        $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); 
    }
    catch (PDOException $e) {        // Kan ikke koble til database-serveren
      echo 'Feil ved tilkobling til databasen: ' . $e->getMessage();
      exit();
    }
  }

  /**
   * Funksjon for å hente ut et pdo-objekt
   * 
   * @return pdo Et pdo-objekt som er koblet til databasen.
   */
  public static function hentDB() {
      if (DB::$db == null) {
        DB::$db = new self();
      }

      return DB::$db->dbh;
  }
}