<?php
/**Dokumenter klassen */



class Bruker {
  private $dbh = null;

  /**
   * Hentet fra prosjket 1 (HLS)
   * Constructor som tar i mot database-objekt, starter session
   *
   * @param PDO $db Et PDO-objekt som er koblet til databasen
   */
  public function __construct($db) {
    $this->dbh = $db;

    session_start();
  }


  /**
   *Sjekker om bruker med gitt brukernavn og passord eksisterer og logger inn
   *
   * @param string $brukernavn Brukernavnet til brukeren
   * @param string $pwd Passordet til brukeren
   * @return bool Returnerer true dersom brukeren ble logget inn, og false om ne gikk galt
   */
  public function loggInn($brukernavn, $pwd) {
    $sql = 'SELECT id, uname, pwd FROM user WHERE uname = ?';
    $sth = $this->dbh->prepare($sql);
    $sth->execute([$brukernavn]);
    $bruker = $sth->fetch(PDO::FETCH_ASSOC);
    if($bruker != null && password_verify($pwd, $bruker['pwd'])) {
      $_SESSION['id'] = $bruker['id'];
      return true;
    }
    else {
      return false;
    }
  }
    /**
   *Lister ut alle registrerte brukere
   *
   * @return array en array med alle registrerte brukere
   */
  public function listBrukere() {
    $sql = 'SELECT id, uname, avatar FROM user';
    $sth = $this->dbh->prepare($sql);
    $sth->execute();
    return ($sth->fetchAll(PDO::FETCH_ASSOC));
  }
  
  /**
   * Inspirert av prosjekt 1 (HLS)
   * Funksjon for å sjekke om brukeren er logget inn.
   *
   * @return int Returnerer id til brukeren dersom brukeren er logget inn. Returnerer 0 dersom brukeren ikke er logget inn.
   */
  public function innlogget() {
    if(isset($_SESSION['id'])) {
      return $_SESSION['id'];
    }
    else {
      return 0;
    }
  }

  /**
   * Funskjon for å hente ut informasjon om brukeren
   * 
   * @param int $brukerId Id til brukeren
   * @return array Returnerer en array med informasjon om brukeren
   */
  public function brukerInformasjon($brukerId) {
    $sql = 'SELECT id, uname, avatar FROM user WHERE id = ?';
    $sth = $this->dbh->prepare($sql);
    $sth->execute([$brukerId]);
    $bruker = $sth->fetch(PDO::FETCH_ASSOC);
    return $bruker;
  }

  /**
   * Funskjon som registrerer ny bruker
   * 
   * @param string $brukernavn ønsket brukernvan
   * @param string $passord passordet (i klartekst)
   * @return array En array med indeksene 'status' og eventuelt 'feilmelding'.
   */
  public function registrer($brukernavn, $passord) {
    if(!$this->brukerEksisterer($brukernavn)) {
      $pwdHash = password_hash($passord, PASSWORD_ARGON2I);  //Bruker den mest annbefalte password hashen (ARGON2I) for best mulig sikkerhet
      $sql = 'INSERT INTO user(uname, pwd) VALUES(?, ?)';
      $sth = $this->dbh->prepare($sql);
      $sth->execute([$brukernavn, $pwdHash]);
      if($sth->rowCount() == 1) {
        $resultat['status'] = 'ok';
        $this->loggInn($brukernavn, $passord);
      }
      else {
        $resultat['status'] = 'feil';
        $resultat['feilmelding'] = 'Noe gikk galt, prøv igjen';
      }
    }
    else {
      $resultat['status'] = 'feil';
      $resultat['feilmelding'] = 'Beklager, dette brukernavnet er allerede tatt';
    }
    return $resultat;
  }

  /**
   * Funskjon som endrer størrelsen på et bilde og lster det opp som brukerens avatar
   * 
   * @param string $brukerid id til brukeren
   * @param ? $bilde bilde som skal rukes som avatar
   */
  public function lastOppAvatar($brukerid, $bilde) {
    //Endre størrelsen på bilde
    //Dersom gammelt bilde, slett
    //Last opp nytt bilde
    //Returner status

    // Inspirert av: https://bitbucket.org/okolloen/imt2291-v2018/src/30012325241c1c3826a3d6e596a459937a3a12d1/uke5_forelesning/image_upload_and_scale_to_db/upload.php?at=master&fileviewer=file-view-default
    if (is_uploaded_file($bilde['tmp_name'])) {
      $content = file_get_contents($bilde['tmp_name']);
      $scaledContent = $this->scale (imagecreatefromstring($content), 150, 150); }
      unset ($content);     // Free up memory from old/unscaled image
      //$mime = $bilde['tmp_name']['type'];
      //$name = $bilde['tmp_name']['name'];
      //$size = $bilde['tmp_name']['size'];
    
      $sql = 'UPDATE user SET avatar = ? WHERE id = ?';
      $sth = $this->dbh->prepare ($sql);
      $sth->execute ([$scaledContent, $brukerid]);
      if($sth->rowCount() == 1) {
        $resultat['status'] = 'ok';
      }
      else {
        $resultat['status'] = 'feil';
        $resultat['feilmelding'] = 'Noe gikk galt, prøv igjen';
      }
      return $resultat;


      /*
      $sql = 'INSERT INTO bilde (navn, thumbnail, mime, str) VALUES (:name, :content, :mime, :size)';
      $sth = $dbh->prepare ($sql);
      $sth->bindParam(':name', $name); // scaled image will be png format
      $sth->bindParam(':mime', $mime);
      $sth->bindParam(':size', $size);
      $sth->bindParam(':content', $scaledContent);
      $sth->execute ();
      if ($sth->rowCount()==1) {
        $mappe = "opplastninger/";   //Dette avsnittet forbereder
        $fil = $mappe . $_FILES["lastOppFil"]["name"];
        if (move_uploaded_file($_FILES["lastOppFil"]["tmp_name"], $fil)) {
          echo "Filen ". basename( $_FILES["lastOppFil"]["name"]). " har blitt lastet opp.";
        } 
        else {
            echo "Kunne ikke laste opp filen.";
        }
      }
    } else {  // Some trickery is going on
      // bad request
    }*/
  // Slutt på inspirert av https://bitbucket.org/okolloen/imt2291-v2018/src/30012325241c1c3826a3d6e596a459937a3a12d1/uke5_forelesning/image_upload_and_scale_to_db/upload.php?at=master&fileviewer=file-view-default
  }



  /**
   * Funskjon som sjekker om brukernavnet allerede eksisterer
   * 
   * @param string $brukernavn Brukernavnet som skal sjekkes om eksiterer
   * @return bool Returnerer true om den eksisterer, og false om den ikke eksisterer
   */
  private function brukerEksisterer($brukernavn) {
    $sql = 'SELECT COUNT(*) AS antall FROM user WHERE uname = ?';
    $sth = $this->dbh->prepare($sql);
    $sth->execute([$brukernavn]);
    return ($sth->fetch(PDO::FETCH_ASSOC)['antall'] == 1);
  }

   /**
   * Funskjon som scalerer et bilde
   * 
   * @param ? orginal bilde
   * @param string $new_width bredden på det nye bilde
   * @param string $new_hight høyden på det nye bilde
   * @return ? scalert bilde
   */
  // Hentet fra https://bitbucket.org/okolloen/imt2291-v2018/src/30012325241c1c3826a3d6e596a459937a3a12d1/uke5_forelesning/image_upload_and_scale_to_db/upload.php?at=master&fileviewer=file-view-default
  private function scale ($img, $new_width, $new_height) {
    $old_x = imageSX($img);
    $old_y = imageSY($img);
  
    if($old_x > $old_y) {                     // Image is landscape mode
      $thumb_w = $new_width;
      $thumb_h = $old_y*($new_height/$old_x);
    } else if($old_x < $old_y) {              // Image is portrait mode
      $thumb_w = $old_x*($new_width/$old_y);
      $thumb_h = $new_height;
    } if($old_x == $old_y) {                  // Image is square
      $thumb_w = $new_width;
      $thumb_h = $new_height;
    }
  
    if ($thumb_w>$old_x) {                    // Don't scale images up
      $thumb_w = $old_x;
      $thumb_h = $old_y;
    }
  
    $dst_img = ImageCreateTrueColor($thumb_w,$thumb_h);
    imagecopyresampled($dst_img,$img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
  
    ob_start();                         // flush/start buffer
    imagepng($dst_img,NULL,9);          // Write image to buffer
    $scaledImage = ob_get_contents();   // Get contents of buffer
    ob_end_clean();                     // Clear buffer
    return $scaledImage;
  }
  // Slutt på hentet fra https://bitbucket.org/okolloen/imt2291-v2018/src/30012325241c1c3826a3d6e596a459937a3a12d1/uke5_forelesning/image_upload_and_scale_to_db/upload.php?at=master&fileviewer=file-view-default
}

