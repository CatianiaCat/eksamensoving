<?php
require_once 'vendor/autoload.php';
require_once 'classes/DB.php';
require_once 'classes/Bruker.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader, array(
    /*'cache' => './compilation_cache',*/ /* Only enable cache when everything works correctly */
));

$data = [];
$dbh = DB::hentDB();
$bruker = new Bruker($dbh);

if($bruker->innlogget()) {
  $data['innlogget'] = true;
  $data['bruker'] = $bruker->brukerInformasjon($bruker->innlogget());
  if(isset($_GET['loggut'])) {
    unset($_SESSION['id']); 
    $data['innlogget'] = false;
  }
}
else {
  if(isset($_POST['navn'], $_POST['pwdForm'])) {
    $data['resultat'] = $bruker->loggInn($_POST['navn'], $_POST['pwdForm']);
    if($data['resultat']) {
      $data['innlogget'] = true;
      $data['bruker'] = $bruker->brukerInformasjon($bruker->innlogget());
    }
    $data['brukernavn'] = $_POST['navn'];
  }
}




echo $twig->render('oppgave2.html', $data);