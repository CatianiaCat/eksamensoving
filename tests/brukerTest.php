<?php
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../classes/Bruker.php';
require_once __DIR__ . '/../classes/DB.php';
use PHPUnit\Framework\TestCase;

class BrukerTest extends TestCase {
  private $bruker = null;
  private $brukernavn = null;
  private $passord = null;

  public function __construct() {
    parent::__construct();

    $this->bruker = new Bruker(DB::hentDB());
    $this->brukernavn = md5(date('l jS \of F Y h:i:s A'));
    $this->passord = md5(date('l jS \of F Y h-i-s A'));
  }

  

  public function testRegistrerOgLoggInn() {
    $res = $this->bruker->registrer($this->brukernavn, $this->passord)['status'];
    $this->assertEquals($res, 'ok');
    $logginn = $this->bruker->loggInn($this->brukernavn, $this->passord); 
    $this->assertTrue($logginn);
  }

  public function testInnloggetOgBrukerInfo() {
    $this->bruker->registrer($this->brukernavn, $this->passord); //Registrer
    $this->bruker->loggInn($this->brukernavn, $this->passord); //LoggInn

    $res = $this->bruker->innlogget();
    $this->assertTrue($res > 0);
    $info = $this->bruker->brukerInformasjon($res);
    $this->assertTrue($res > 0);
  }

  public function testListBrukere() {
    $this->bruker->registrer($this->brukernavn, $this->passord); //Registrer
    $this->bruker->loggInn($this->brukernavn, $this->passord); //LoggInn

    $res = $this->bruker->listBrukere();
    $this->assertTrue($res > 0);
    $brukerdata = [
      'id' => $this->bruker->innlogget(), 
      'uname' => $this->brukernavn, 
      'avatar' => null
    ];
    $this->assertContains($brukerdata, $res);
  }
}

