<?php
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../classes/Bruker.php';
require_once __DIR__ . '/../classes/DB.php';

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use Behat\Mink\Element\DocumentElement;
use Behat\Mink\Element\NodeElement;

class FunctionalTests extends TestCase { 
  /* Change this to suite your server setup */
  protected $baseUrl = "http://localhost/www/Eksamen2017/";
  protected $session; //?

  private $kontakt = NULL; //?

  /**
   * Initiates the testing session, this is done before each test.
   * Starts a new session.
   */
  protected function setup() {
    $driver = new \Behat\Mink\Driver\GoutteDriver();
    $this->session = new \Behat\Mink\Session($driver);
    $this->kontakt = [
      'brukernavn' => md5(date('l jS \of F Y h:i:s A')),
      'passord' => md5(date('l jS \of F Y h-i-s A'))
    ];

  }

  /*public function testoppgave1() {
    $this->session->visit($this->baseUrl . 'oppgave1.php');
    $page = $this->session->getPage();
    
  }*/ //Burde muligens leges til en sjekk som sjekker om databasen allerede eksisterer

  public function testoppgave2og3og5() {
    //Registrer
    $this->session->visit($this->baseUrl . 'oppgave3.php');
    $page = $this->session->getPage();

    $form = $page->find('css', 'form[method="post"]');
    $input = $page->find('css', '#navn');
    $input->setValue($this->kontakt['brukernavn']);
    $input = $page->find('css', '#pwdForm');
    $input->setValue($this->kontakt['passord']);
    $input = $page->find('css', '#GPwdForm');
    $input->setValue($this->kontakt['passord']);
    $form->submit();

    $this->session->visit($this->baseUrl . 'oppgave2.php');
    $page = $this->session->getPage();
    $knapp = $page->find('css', 'a');
    $this->assertEquals('Logg ut', $knapp->getText());

    //Logg ut
    $this->session->visit($this->baseUrl . 'oppgave2.php?loggut=true');
    $page = $this->session->getPage();
    $knapp = $page->find('css', 'button');
    $this->assertEquals('Logg inn', $knapp->getText());

    //Logg inn
    $form = $page->find('css', 'form[method="post"]');
    $input = $page->find('css', '#navn');
    $input->setValue($this->kontakt['brukernavn']);
    $input = $page->find('css', '#pwdForm');
    $input->setValue($this->kontakt['passord']);
    $form->submit();

    $knapp = $page->find('css', 'a');
    $this->assertEquals('Logg ut', $knapp->getText());


    //List ut registrerte brukere
    $this->session->visit($this->baseUrl . 'oppgave5.php');
    $page = $this->session->getPage();

    $text = $page->find('named', ['content', $this->kontakt['brukernavn']]);
    $this->assertEquals($this->kontakt['brukernavn'], $text->getText());
  }

}
