<?php

try {
  $dbh = new PDO("mysql:dbname=;host=127.0.0.1", 'root', ''); //Kobler til databasen
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); 
}
catch (PDOException $e) {
  die($e);
}

//Legge inn noe som sjekker om databsenen eksisterer fra før av
$sql = 'CREATE DATABASE imt2291eksamen470823;
USE imt2291eksamen470823;
CREATE TABLE `imt2291eksamen470823`.`user` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `uname` VARCHAR(32) NOT NULL,
  `pwd` VARCHAR(255) NOT NULL,
  `avatar` BLOB, 
  PRIMARY KEY (`id`), 
  UNIQUE (`uname`)
) ENGINE = InnoDB;';
$sth = $dbh->prepare($sql);
if($sth->execute()) {
  echo ('Har opprettet databasen');
}

$brukernavn = 'test';
$pwdHash = password_hash('test', PASSWORD_ARGON2I);  //Bruker den mest annbefalte password hashen (ARGON2I) for best mulig sikkerhet
$sql = 'INSERT INTO user(uname, pwd) VALUES(?, ?)';
$sth = $dbh->prepare($sql);
if($sth->execute([$brukernavn, $pwdHash])) {
  echo ('Brukeren Test er lagt til');
}




