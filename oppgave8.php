<?php
require_once 'vendor/autoload.php';
require_once 'classes/DB.php';
require_once 'classes/Bruker.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader, array(
    /*'cache' => './compilation_cache',*/ /* Only enable cache when everything works correctly */
));

$data = [];
$dbh = DB::hentDB();


$sql = 'SELECT subject, type, studyprogramcontent.semester AS semestere, subject.name AS fag, credits, studyprogram.name AS linje, subject.semester FROM `studyprogramcontent` 
INNER JOIN subject ON studyprogramcontent.subject = subject.code
INNER JOIN studyprogram ON studyprogramcontent.studyprogram = studyprogram.id
GROUP BY subject';
$sth = $dbh->prepare($sql);
$sth->execute();
$data['row'] = $sth->fetchAll(PDO::FETCH_ASSOC);

foreach($data['row'] as &$typer) {
  if($typer['type'] == 'obligatory') {
    $typer['type'] = 'O';
  }
  else {
    $typer['type'] = 'V';
  }
}
unset($typer);



//print_r($data);





echo $twig->render('oppgave8.html', $data);