<?php
require_once 'classes/DB.php';
require_once 'classes/Bruker.php';


$dbh = DB::hentDB();
$bruker = new Bruker($dbh);


$data = $bruker->listBrukere();

foreach($data as &$person) {
  if($person['avatar'] == null) {
    $person['avatar'] = 'n';
  }
  else {
    $person['avatar'] = 'y';
  }
}
unset($person);

header('Content-Type: application/json');
echo json_encode($data);