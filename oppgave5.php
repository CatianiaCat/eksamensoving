<?php
require_once 'vendor/autoload.php';
require_once 'classes/DB.php';
require_once 'classes/Bruker.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader, array(
    /*'cache' => './compilation_cache',*/ /* Only enable cache when everything works correctly */
));

$data = [];
$dbh = DB::hentDB();
$bruker = new Bruker($dbh);

$data['brukere'] = $bruker->listBrukere();

echo $twig->render('oppgave5.html', $data);
?>