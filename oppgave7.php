<?php
require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader, array(
    /*'cache' => './compilation_cache',*/ /* Only enable cache when everything works correctly */
));
$data = [];

$data = file_get_contents("http://api.geonames.org/postalCodeSearchJSON?placename=gj%C3%B8vik&country=NO&maxRows=30&username=okolloen");
$data = json_decode($data, true);

echo $twig->render('oppgave7.html', $data);