<?php
 /**
  * IKKE GJORT
  * 1. Se om du kan skifte farge fra blå til rød når det ikke er riktfi (gjelder den andre også) 
  */

  require_once 'vendor/autoload.php';
  require_once 'classes/DB.php';
  require_once 'classes/Bruker.php';
  
  $loader = new Twig_Loader_Filesystem('./templates');
  $twig = new Twig_Environment($loader, array(
      /*'cache' => './compilation_cache',*/ /* Only enable cache when everything works correctly */
  ));
  
  $data = [];
  $dbh = DB::hentDB();
  $bruker = new Bruker($dbh);

  if(isset($_POST['navn'], $_POST['pwdForm'])) {
    $data['resultat'] = $bruker->registrer($_POST['navn'], $_POST['pwdForm']);
    if($data['resultat']['status'] == 'ok') {
      header('Location: oppgave2.php');
      exit();
    }
    else {
      $data['brukernavn'] = $_POST['navn'];
    }
  }
  echo $twig->render('oppgave3.html', $data);




